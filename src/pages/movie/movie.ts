import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { MoviesProvider } from '../../providers/movies/movies';
import { MovieDetailPage } from '../movie-detail/movie-detail';

@IonicPage()
@Component({
  selector: 'page-movie',
  templateUrl: 'movie.html',
  providers: [
    MoviesProvider,
  ]
})
export class MoviePage {
  public movieList = new Array<any>();
  public loader;
  public refresher;
  public isRefreshing: boolean = false;
  public page = 1;
  public infiniteScroll;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private moviesProvider: MoviesProvider,
    private loadingCtrl: LoadingController
  ) {
  }

  doInfinite(infiniteScroll) {
    this.page++;
    this.infiniteScroll = infiniteScroll;

    this.loadPopular(true);
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: 'Carregando...',
    });
    this.loader.present();
  }
  closeLoading() {
    this.loader.dismiss();
  }

  doRefresh(refresher) { //Mostra indicador de atualização
    this.page = 1;
    this.refresher = refresher;
    this.isRefreshing = true; // Indica para o programa que está atualizando
    this.loadPopular();
  }

  loadPopular(newPage: boolean = false) {
    this.moviesProvider.getPopularMovies(this.page).subscribe(
      data => {
        const response = (data as any).results;

        if (newPage) {
          this.movieList = this.movieList.concat(response);
          this.infiniteScroll.complete(); //Toda vez que é uma nova pág. preciso fechar o infiniteScroll
        } else {
          this.movieList = response;
        }

        if (this.isRefreshing) { //Após carregar os filmes novamente...
          this.refresher.complete(); //fechamos o indicador de atualização
          this.isRefreshing = false; //indicamos p/ o programa que o carregamento acabou
        }
      }, error => {
        console.log(error);
      }
    )
  }
  

  ionViewDidLoad() { //Executa apenas uma vez para cada inicialização do app.
    console.log('ionViewDidEnter MoviePage');
    //this.page = 1;
    //A caixa de carregamento é aberta e fechada fora da função loadMovies()
    // para que ela não apareça quando o usuário estiver atualizando a lista de filmes
    this.presentLoading() //Abre a caixa de carregamento
    this.loadPopular();
    this.closeLoading(); //Fecha caixa de carregamento
  }

  openDetails(film) {
    this.navCtrl.push(MovieDetailPage, { id: film.id });
  }
}
