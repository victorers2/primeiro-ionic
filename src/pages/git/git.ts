import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { GithubProvider } from '../../providers/github/github';
import { GitDetailPage } from '../git-detail/git-detail';

@IonicPage()
@Component({
  selector: 'page-git',
  templateUrl: 'git.html',
  providers: [
    GithubProvider,
  ]
})
export class GitPage {
  username: string = '';
  usernames:string [];
  items = [];
  instruction:boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public gitProvider: GithubProvider,
    public toast: ToastController) {
  }

  getItems(ev: any) {
    if(this.instruction){
      this.instruction=false;
      //this.presentToast();
    }
    // set val to the value of the searchbar
    const val:string = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.gitProvider.getUserList(val).subscribe(
        data => {
          this.items = (data as any).items;
        }, error => {
          console.log("Erro: " + error);
        }
      )
    } else {
      this.items = [];
    }
  }

  presentToast() {
    const t = this.toast.create({
      message: "Clique em um perfil para ver detalhes",
      showCloseButton: true,
      closeButtonText: 'Ok',
      position: "top",
    })
    t.present();
  }

  userDetail(userName) {
    this.navCtrl.push(GitDetailPage, {userName: userName});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GitPage');
  }
}
