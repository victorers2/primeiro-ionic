import { Component, ViewChild } from '@angular/core';

import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IntroPage } from '../intro/intro';
import { MoviePage } from '../movie/movie';
import { ClimaPage } from '../clima/clima';
import { GitPage } from '../git/git';
import { SuperTabsController } from 'ionic2-super-tabs';
import { SuperTabsModule} from 'ionic2-super-tabs';


@IonicPage()
@Component({
  templateUrl: 'tabs.html',
  providers: []
})
export class TabsPage {

  selectedTab = 0;
  tab1Root = IntroPage;
  tab2Root = MoviePage;
  tab3Root = ClimaPage;
  tab4Root = GitPage;

  tabs = [
    {root: this.tab2Root, title:"Filmes", id:"filme", icon: 'play'},
    {root: this.tab3Root, title:"Clima", id:"clima", icon: 'partly-sunny'},
    {root: this.tab4Root, title:"GitHub", id:"git", icon: 'logo-github'},
  ]

  @ViewChild(SuperTabsController) superTabs:SuperTabsController;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
  }

  onTabSelection(ev: any) {
    this.selectedTab = ev.index;
  }
}
