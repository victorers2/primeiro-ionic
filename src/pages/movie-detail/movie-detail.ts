import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MoviesProvider } from '../../providers/movies/movies';


@IonicPage()
@Component({
  selector: 'page-movie-detail',
  templateUrl: 'movie-detail.html',
  providers: [
    MoviesProvider,
  ]
})
export class MovieDetailPage {
  public film;
  public filmeId;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public moviesProvider: MoviesProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MovieDetailPage');
    this.filmeId = this.navParams.get("id");
    this.moviesProvider.getDetails(this.filmeId).subscribe(
      data => {
        this.film = (data as any);
      }, error => {
        console.log(error);
      }
    )
  }

}
