import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GithubProvider } from '../../providers/github/github';

@IonicPage()
@Component({
  selector: 'page-git-detail',
  templateUrl: 'git-detail.html',
  providers: [
    GithubProvider
  ]
})
export class GitDetailPage {
  userData;
  repoData;
  section:string = "visao";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public gitProvider: GithubProvider) {
  }

  //Sai do perfil e volta para tela de pesquisar usuários
  goBack() {
    this.navCtrl.popToRoot();
  }

  //Carrega os dados do usuário do github cujo nome foi passado pela página anterior
  ionViewWillEnter() {
    let name = this.navParams.get("userName");
    console.log("Nome do usuário: " + name);
    this.gitProvider.getUserData(this.navParams.get("userName")).subscribe(
      data => {
        this.userData = data as any;
        console.log("Data: " + this.userData.bio);

      }, error => {
        console.log("Error: " + error);
      }
    )
    console.log('ionViewWillEnter GitDetailPage');
  }
}
