import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GitDetailPage } from './git-detail';

@NgModule({
  declarations: [
    GitDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(GitDetailPage),
  ],
})
export class GitDetailPageModule {}
