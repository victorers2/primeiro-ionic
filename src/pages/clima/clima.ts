import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, LoadingController } from 'ionic-angular';
import { WeatherProvider } from '../../providers/weather/weather';

@IonicPage()
@Component({
  selector: 'page-clima',
  templateUrl: 'clima.html',
})
export class ClimaPage {
  @ViewChild(Content) content: Content;

  clima: string = "Hoje";

  //private page:number = 0; //0=hoje, 1=previsão
  public states = ["AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"];
  public cities; //lista com dados de toda as as cidades do estado selecionado (id, name, state, country)
  public city; //dado de uma cidade (id, name, state, country)
  public forecast; //lista com previsões de 15 da cidade selecionada

  public loader; //controlador de carregamento
  public refresher; // controlador de atualização
  public isRefreshing: boolean = false; // indicador de atualização; true = carregando , false = não está carregando

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public weatherProvider: WeatherProvider,
    private loadingCtrl: LoadingController) {
  }

  doRefresh(refresher) { //Mostra indicador de atualização
    this.refresher = refresher; // Só pra fazer a variável ser global
    this.isRefreshing = true; // Indica para o programa que está atualizando
    this.loadWeather(this.city.id);
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: 'Carregando...',
    });
    this.loader.present();
  }
  closeLoading() {
    this.loader.dismiss();
  }

  //Após escolher a cidade, carrega a previsão de 15 dias incluindo hoje e guarda em this.forecast
  loadWeather(cityID) { //TODO: acho q o bug está aqui
    this.presentLoading();
    this.weatherProvider.getCityByID(cityID).subscribe(
      data => {
        this.city = data;
      }, error=> {
        console.log(error);
      }
    );

    this.weatherProvider.getForecast(cityID).subscribe(
      resp => {
        this.forecast = (resp as any).data; //forecast[n] = previsão para o dia n da cidade cujo ID foi dado
        //console.log("Previsão de 15 dias: " + this.forecast[0].date);
      }
    );

    if (this.isRefreshing) { //Após carregar os filmes novamente...
      this.refresher.complete(); //fechamos o indicador de atualização
      this.isRefreshing = false; //indicamos p/ o programa que o carregamento acabou
    }
    this.closeLoading();
  }

  //Após escolher o estado, carrega a lista com nome de suas cidades em 'this.cities'
  loadCities(stateName) {
    this.weatherProvider.getCitiesData(stateName).subscribe(
      data => {
        this.cities = data;
        console.log(this.cities[0]);
      }, error => {
        console.log(error);
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClimaPage');
  }
}
