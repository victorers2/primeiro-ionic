import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClimaPage } from './clima';

@NgModule({
  declarations: [
    ClimaPage,
  ],
  imports: [
    IonicPageModule.forChild(ClimaPage),
  ],
})
export class ClimaPageModule {}
