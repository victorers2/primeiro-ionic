import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { IntroPage } from '../pages/intro/intro';
import { TabsPage } from '../pages/tabs/tabs';
import { ConfigProvider } from '../providers/config/config';

@Component({
  templateUrl: 'app.html',
  providers: [
    ConfigProvider,
  ]
})
export class MyApp {
  rootPage: any;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public storage: Storage,
    public configProvider: ConfigProvider) {
    platform.ready().then(() => { // Executado antes de carregar qualquer coisa em tela

      this.init();
      //this.init();
      //statusBar.styleDefault();
      statusBar.overlaysWebView(true);  // statusBar.hide()
      splashScreen.hide();
    });
  }

  async ini() {
    let config = await this.configProvider.getConfig();

    if (config == undefined || config == null) { // Primeira vez que o app é aberto, o storage ainda não existe
      console.log('config == null = ' + config);
      this.rootPage = IntroPage;
      console.log('this.rootPage = IntroPage');
      await this.configProvider.setConfig();
    } else {
      console.log("config != null = " + config);
      this.rootPage = TabsPage;
      console.log('this.rootPage = TabsPage');
    }
  }

  async init() { // <-- Função inicío sendo usada
    await this.storage.get('introPage').then(
      async data => {
        console.log("data: " + data);
        if (data == null || data == undefined || data == false) { //JAMAIS mudar essa linha. Desse jeito consegui fazer os sliders aparecerem só uma vez.
          console.log("this.rootPage = IntroPage");
          this.rootPage = IntroPage;
          await this.storage.set('introPage', 'falso');
        } else {
          console.log("this.rootPage = TabsPage");
          this.rootPage = TabsPage;
        }
      }, error => {
        console.log(error);
      }
    );
    await this.storage.get('introPage').then(data => {
      console.log("data: " + data);
    });
  }
}
