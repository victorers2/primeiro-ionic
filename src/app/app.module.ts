import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { CommonModule } from "@angular/common";
import { IonicStorageModule } from '@ionic/storage';
import { SuperTabsModule} from 'ionic2-super-tabs';

import { TabsPage } from '../pages/tabs/tabs';
import { IntroPage } from '../pages/intro/intro';
import { MoviePage } from '../pages/movie/movie';
import { ClimaPage } from '../pages/clima/clima';
import { GitPage } from '../pages/git/git';
import { MovieDetailPage } from '../pages/movie-detail/movie-detail';
import { GitDetailPage } from '../pages/git-detail/git-detail';

import { IntroPageModule } from '../pages/intro/intro.module';
import { MoviePageModule } from '../pages/movie/movie.module';
import { ClimaPageModule } from '../pages/clima/clima.module';
import { GitPageModule } from '../pages/git/git.module';
import { MovieDetailPageModule } from '../pages/movie-detail/movie-detail.module';
import { GitDetailPageModule } from '../pages/git-detail/git-detail.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http'; //Necessário para usar HttpModule
import { HttpModule } from '@angular/http';
import { MoviesProvider } from '../providers/movies/movies';
import { WeatherProvider } from '../providers/weather/weather';
import { GithubProvider } from '../providers/github/github';

@NgModule({
  declarations: [
    MyApp,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    SuperTabsModule.forRoot(),
    IntroPageModule,
    MoviePageModule,
    ClimaPageModule,
    GitPageModule,
    MovieDetailPageModule,
    GitDetailPageModule,
    HttpClientModule, //Necessário para usar HttpModule
    HttpModule,
    CommonModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    IntroPage,
    MoviePage,
    ClimaPage,
    GitPage,
    MovieDetailPage,
    GitDetailPage,
  ],
  providers: [ //Providers compartilhados
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    MoviesProvider,
    WeatherProvider,
    GithubProvider
  ]
})
export class AppModule { }
