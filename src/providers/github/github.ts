import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class GithubProvider {
  baseURL: string = "https://api.github.com/";

  constructor(public http: HttpClient) {
    console.log('Hello GithubProvider Provider');
  }

  //Recupera dados públicos de um usuário cujo nome foi dado
  getUserData(userName: string) {
    return this.http.get(`${this.baseURL}users/${userName}`);
  }

  //Recupera informações dos repositórios do usuário cujo nome foi dado
  getUserRepos(userName: string) {
    return this.http.get(`${this.baseURL}users/${userName}/repos`)
  }

  //Recupera lista de usuários com nome parecido ou igual ao nome dado. Deve ser usado para pesquisar no github.
  getUserList(userName: string) {
    return this.http.get(`${this.baseURL}search/users?q=${userName}`);
  }

}
