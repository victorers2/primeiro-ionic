import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class WeatherProvider {
  private baseURL:string = "http://apiadvisor.climatempo.com.br/api/v1/";
  private apiKey: string = "9412c60ead7b3df048d84b05673ebe25"; //TODO: guardar em local mais seguro

  constructor(public http: HttpClient) {
    console.log('Hello WeatherProvider Provider');
  }

  //Recupera IDs das cidades cujo estado foi dado
  getCitiesData(stateName) {
    return this.http.get(`${this.baseURL}locale/city?state=${stateName}&token=${this.apiKey}`);
  }

  //Recupera dados da cidade cujo ID foi dado
  getCityByID(cityID) {
    return this.http.get(`${this.baseURL}locale/city/${cityID}?token=${this.apiKey}`)
  }

  //Recupera previsão da cidade cujo ID foi dado
  getClimate(cityID) {
    return this.http.get(`${this.baseURL}climate/rain/locale/${cityID}?token=${this.apiKey}`);
  }

  //Recupera previsão da cidade para hoje e os próximos 14 dias
  getForecast(cityID) {
    return this.http.get(`${this.baseURL}forecast/locale/${cityID}/days/15?token=${this.apiKey}`);
  }
}
