import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable() //Injactable significa que nunca precisaremos criar um objeto dessa classe. Só usaremos dentro de outra classe.
export class MoviesProvider {
  private baseURL: string = "https://api.themoviedb.org/3/";
  private language: string = "pt-BR";
  private apiKey: string = "a6e84e29bde0da8c6cb47f6e5f71d8f4"; // TODO: esconder me outro arquivo

  constructor(public http: HttpClient) {
    console.log('Hello MoviesProvider Provider');
  }

  getPopularMovies(page = 1) {
    return this.http.get(`${this.baseURL}movie/popular?page=${page}&api_key=${this.apiKey}&language=${this.language}`);
  }

  getDetails(filmId) {
    return this.http.get(`${this.baseURL}movie/${filmId}?api_key=${this.apiKey}&language=${this.language}`);
  }

}