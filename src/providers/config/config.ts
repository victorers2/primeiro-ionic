import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class ConfigProvider {

  configKeyName: string = 'config';
  private config = {
    showIntro: 'true', //true = Mostrar página introdutória, false = Pular página introdutória
    name: '',
    language: 'pt-BR',
  }

  constructor(private storage:Storage) {
  }

  //Recupera configurações do App
  async getConfig() {
    await this.storage.get(this.configKeyName).then(
      data=> {
      //console.log('consultei config: ' + data);
      return JSON.parse(data);
    }).catch(error=> {
      console.log('Houve um Erro em getConfig.');
      return -1;
    });
  }

  //Altera configurações do App
  async setConfig(showIntro:string = 'false', name?:string, language?:string) {
    if(showIntro) 
      //console.log('alterando showIntro p/ ' + showIntro);
      this.config.showIntro = showIntro;
    
    if(name) 
      this.config.name = name;
    
    if(language) 
      this.config.language = language;
    

    await this.storage.set(this.configKeyName, JSON.stringify(this.config));

    await this.storage.get(this.configKeyName).then(data =>{
      console.log("atualizei config p/: " + data);
    })
    
  }
}